package utils

import (
	"reflect"
	"strings"
)

func GetErrorFields(errors error) string {
	keys := reflect.ValueOf(errors).MapKeys()
	strkeys := make([]string, len(keys))
	for i := 0; i < len(keys); i++ {
		strkeys[i] = keys[i].String()
	}
	return strings.Join(strkeys, ", ")
}
