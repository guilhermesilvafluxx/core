package model

import (
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"bitbucket.org/guilhermesilvafluxx/core/fxerror"
	"bitbucket.org/guilhermesilvafluxx/core/utils"
	"net/http"
	"regexp"
	"time"
)

type Contract struct {
	ClientId                 string             `json:"clientId,omitempty" bson:"clientId,omitempty"`
	Amount                   Amount             `json:"amount,omitempty" bson:"amount,omitempty"`
	Fullname                 string             `json:"fullname,omitempty" bson:"fullname,omitempty"`
	Email                    string             `json:"email,omitempty" bson:"email,omitempty"`
	BirthDate                string             `json:"birthDate,omitempty" bson:"birthDate,omitempty"`
	IdDocument               IdDocument         `json:"idDocument,omitempty" bson:"idDocument,omitempty"`
	Gender                   string             `json:"gender,omitempty" bson:"gender,omitempty"`
	CivilState               string             `json:"civilState,omitempty" bson:"civilState,omitempty"`
	BirthState               string             `json:"birthState,omitempty" bson:"birthState,omitempty"`
	BirthCity                string             `json:"birthCity,omitempty" bson:"birthCity,omitempty"`
	TaxDocument              TaxDocument        `json:"taxDocument,omitempty" bson:"taxDocument,omitempty"`
	Phone                    Phone              `json:"phone,omitempty" bson:"phone,omitempty"`
	Cellphone                Phone              `json:"cellphone,omitempty" bson:"cellphone,omitempty"`
	MotherFullname           string             `json:"motherFullname,omitempty" bson:"motherFullname,omitempty"`
	Job                      Job                `json:"job,omitempty" bson:"job,omitempty"`
	PoliticallyExposedPerson bool               `json:"politicallyExposedPerson" bson:"politicallyExposedPerson"`
	Annotaded                bool               `json:"annotaded" bson:"annotaded"`
	Risk                     string             `json:"risk" bson:"risk"`
	Addresses                []Address          `json:"addresses,omitempty" bson:"addresses,omitempty"`
	Banking                  Banking            `json:"banking,omitempty" bson:"banking,omitempty"`
	Items                    []Item             `json:"items,omitempty" bson:"items,omitempty"`
	ProposalResponses        []ProposalResponse `json:"proposalResponses,omitempty" bson:"proposalResponses,omitempty"`
	Notified                 bool               `json:"notified,omitempty" bson:"notified,omitempty"` // True se ja recebeu todas as 3 notificacoes
	Deceased                 bool               `json:"deceased,omitempty" bson:"deceased,omitempty"`
	NotificationContext      string             `json:"notificationContext,omitempty" bson:"notificationContext,omitempty"` // RECOVERY_SIGNIN, RECOVERY_PARTNER, CONTRACT_CONFIRM
	MerchantId               string             `json:"merchantId,omitempty" bson:"merchantId,omitempty"`
	CreatedAt                time.Time          `json:"createdAt,omitempty" bson:"createdAt,omitempty"`
	UpdatedAt                time.Time          `json:"updatedAt,omitempty" bson:"updatedAt,omitempty"`
}

type IdDocument struct {
	Number        string `json:"number,omitempty" bson:"number,omitempty"`
	Origin        string `json:"origin,omitempty" bson:"origin,omitempty"`
	EmissionDate  string `json:"emissionDate,omitempty" bson:"emissionDate,omitempty"`
	EmissionState string `json:"emissionState,omitempty" bson:"emissionState,omitempty"`
}

type Job struct {
	Income         int32  `json:"income,omitempty" bson:"income,omitempty"`
	Description    string `json:"description,omitempty" bson:"description,omitempty"`
	Position       string `json:"position,omitempty" bson:"position,omitempty"`
	Occupation     string `json:"occupation,omitempty" bson:"occupation,omitempty"`
	EducationLevel string `json:"educationLevel,omitempty" bson:"educationLevel,omitempty"`
	Company        string `json:"company,omitempty" bson:"company,omitempty"`
}

type Banking struct {
	BankId       int32  `json:"bankId,omitempty" bson:"bankId,omitempty"`
	Account      int32  `json:"account,omitempty" bson:"account,omitempty"`
	AccountDigit int32  `json:"accountDigit,omitempty" bson:"accountDigit,omitempty"`
	AccountType  string `json:"accountType,omitempty" bson:"accountType,omitempty"`
	Agency       int32  `json:"agency,omitempty" bson:"agency,omitempty"`
	AgencyDigit  int32  `json:"agencyDigit,omitempty" bson:"agencyDigit,omitempty"`
	Payday       int64  `json:"payday,omitempty" bson:"payday,omitempty"`
}

func (c Contract) Validate() error {
	err := validation.Errors{
		"fluxxId":                  validation.Validate(&c.ClientId, validation.Length(1, 45)),
		"amount":                   validation.Validate(&c.Amount, validation.Required),
		"fullname":                 validation.Validate(&c.Fullname, validation.Length(1, 90)),
		"email":                    validation.Validate(&c.Email, validation.Required, validation.Length(1, 90)),
		"birthDate":                validation.Validate(&c.BirthDate, validation.Required, validation.Match(regexp.MustCompile(`([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))`))),
		"idDocument":               validation.Validate(&c.IdDocument, validation.Required),
		"idDocument.number":        validation.Validate(&c.IdDocument.Number, validation.Required, validation.Length(1, 20)),
		"idDocument.origin":        validation.Validate(&c.IdDocument.Origin, validation.Required, validation.Length(1, 20)),
		"idDocument.emissionDate":  validation.Validate(&c.IdDocument.EmissionDate, validation.Required, validation.Match(regexp.MustCompile(`([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))`))),
		"idDocument.emissionState": validation.Validate(&c.IdDocument.EmissionState, validation.Required, validation.Length(2, 2)),
		"gender":                   validation.Validate(&c.Gender, validation.Required, validation.Length(1, 2)),
		"civilState":               validation.Validate(&c.CivilState, validation.Required, validation.Length(1, 20)),
		"taxDocument":              validation.Validate(&c.TaxDocument, validation.Required),
		"taxDocument.type":         validation.Validate(&c.TaxDocument.Type, validation.Required, validation.In("CPF", "CNPJ")),
		"taxDocument.number":       validation.Validate(&c.TaxDocument.Number, validation.Required, validation.Length(1, 11)),
		"phone":                    validation.Validate(&c.Phone, validation.Required),
		"phone.number":             validation.Validate(&c.Phone.Number, validation.Required, validation.Max(999999999)),
		"phone.areaCode":           validation.Validate(&c.Phone.AreaCode, validation.Required, validation.Max(99)),
		"phone.countryCode":        validation.Validate(&c.Phone.CountryCode, validation.Required, validation.Max(99)),
		"cellphone":                validation.Validate(&c.Phone, validation.Required),
		"cellphone.number":         validation.Validate(&c.Cellphone.Number, validation.Required, validation.Max(999999999)),
		"cellphone.areaCode":       validation.Validate(&c.Cellphone.AreaCode, validation.Required, validation.Max(99)),
		"cellphone.countryCode":    validation.Validate(&c.Cellphone.CountryCode, validation.Required, validation.Max(99)),
		"motherFullname":           validation.Validate(&c.MotherFullname, validation.Required, validation.Length(1, 90)),
		"job":                      validation.Validate(&c.Job, validation.Required),
		"job.description":          validation.Validate(&c.Job.Description, validation.Required, validation.Length(1, 90)),
		"job.position":             validation.Validate(&c.Job.Position, validation.Required, validation.Length(1, 90)),
		"job.occupation":           validation.Validate(&c.Job.Occupation, validation.Required, validation.Length(1, 90)),
		"job.educationLevel":       validation.Validate(&c.Job.EducationLevel, validation.Required, validation.Length(1, 90)),
		"addresses":                validation.Validate(&c.Addresses),
		"banking":                  validation.Validate(&c.Banking, validation.Required),
		"banking.bankId":           validation.Validate(&c.Banking.BankId, validation.Required, validation.Max(9999)),
		"banking.account":          validation.Validate(&c.Banking.Account, validation.Required, validation.Max(9999999999)),
		"banking.accountDigit":     validation.Validate(&c.Banking.AccountDigit, validation.Required, validation.Max(9)),
		"banking.agency":           validation.Validate(&c.Banking.Agency, validation.Required, validation.Max(9999999999)),
		"banking.agencyDigit":      validation.Validate(&c.Banking.AgencyDigit, validation.Required, validation.Max(9)),
		"items":                    validation.Validate(&c.Items),
	}.Filter()

	// Gets fields that have error
	var errFields string
	if err != nil {
		errFields = utils.GetErrorFields(err)
		return fxerror.NewError("invalid-field", fxerror.InvalidField, errFields, "", http.StatusBadRequest, fxerror.InvalidRequest, err)
	}

	return nil
}
