package model

type RebelOffer struct {
	AnnualEcdRate        float64 `json:"annualEcdRate" bson:"annualEcdRate"`
	AnnualInterestRate   float64 `json:"annualInterestRate" bson:"annualInterestRate"`
	Fee                  float64 `json:"fee" bson:"fee"`
	InstallmentValue     float64 `json:"installmentValue" bson:"installmentValue"`
	InstallmentsDueDay   int64   `json:"installmentsDueDay" bson:"installmentsDueDay"`
	LoanDate             string  `json:"loanDate" bson:"loanDate"`
	LoanValue            int64   `json:"loanValue" bson:"loanValue"`
	MonthlyEcdRate       float64 `json:"monthlyEcdRate" bson:"monthlyEcdRate"`
	MonthlyInterestRate  float64 `json:"monthlyInterestRate" bson:"monthlyInterestRate"`
	NumberOfInstallments int64   `json:"numberOfInstallments" bson:"numberOfInstallments"`
	OneTimeFineRate      int64   `json:"onetimeFineRate" bson:"onetimeFineRate"`
	Taxes                float64 `json:"taxes" bson:"taxes"`
}

type RebelLoan struct {
	ApplicationId        int64        `json:"applicationId,omitempty" bson:"applicationId,omitempty"`
	DesiredPrincipal     float64      `json:"desiredPrincipal" bson:"desiredPrincipal"`
	InstallmentsDueDay   int64        `json:"installmentsDueDay" bson:"installmentsDueDay"`
	MaxInstallmentsValue float64      `json:"maxInstallmentsValue" bson:"maxInstallmentsValue"`
	MinInstallmentValue  float64      `json:"minInstallmentValue" bson:"minInstallmentValue"`
	MaxTerm              int64        `json:"maxTerm" bson:"maxTerm"`
	MaxTotalValue        float64      `json:"maxTotalValue" bson:"maxTotalValue"`
	MinTotalValue        float64      `json:"minTotalValue" bson:"minTotalValue"`
	MonthlyInterestRate  float64      `json:"monthlyInterestRate" bson:"monthlyInterestRate"`
	Suggestions          []RebelOffer `json:"suggestions,omitempty" bson:"suggestions,omitempty"`
	SelectedOffer        RebelOffer   `json:"selectedOffer,omitempty" bson:"selectedOffer,omitempty"`
	Status               string       `json:"status,omitempty" bson:"status,omitempty"`
	URL                  string       `json:"url,omitempty" bson:"url,omitempty"`
}
