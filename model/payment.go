package model

import (
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"bitbucket.org/guilhermesilvafluxx/core/fxerror"
	"bitbucket.org/guilhermesilvafluxx/core/utils"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http"
	"time"
)

type CancellationDetails struct {
	CancelledBy string `json:"cancelledBy,omitempty" bson:"cancelledBy,omitempty"`
	Code        int32  `json:"code,omitempty" bson:"code,omitempty"`
	Description string `json:"description,omitempty" bson:"description,omitempty"`
}

type Geolocation struct {
	Latitude  float64 `json:"latitude,omitempty" bson:"latitude,omitempty"`
	Longitude float64 `json:"longitude,omitempty" bson:"longitude,omitempty"`
}

type Device struct {
	IP          string      `json:"ip,omitempty" bson:"ip,omitempty"`
	UserAgent   string      `json:"userAgent,omitempty" bson:"userAgent,omitempty"`
	Geolocation Geolocation `json:"geolocation,omitempty" bson:"geolocation,omitempty"`
}

type Payment struct {
	Id                  primitive.ObjectID  `json:"_id,omitempty" bson:"_id,omitempty"`
	WirecardPayId       string              `json:"wirecardPayId,omitempty" bson:"wirecardPayId,omitempty"`
	OwnId               string              `json:"ownId,omitempty" bson:"ownId,omitempty"`
	CheckoutId          primitive.ObjectID  `json:"-" bson:"checkoutId,omitempty"`
	Status              string              `json:"status,omitempty" bson:"status,omitempty"`
	Amount              Amount              `json:"amount,omitempty" bson:"amount,omitempty"`
	CancellationDetails CancellationDetails `json:"cancellationDetails,omitempty" bson:"cancellationDetails,omitempty"`
	Device              Device              `json:"device,omitempty" bson:"device,omitempty"`
	Fluxx               Fluxx               `json:"fluxx,omitempty" bson:"fluxx,omitempty"`   // This is updated after sending webhook
	Boleto              BoletoDetails       `json:"boleto,omitempty" bson:"boleto,omitempty"` // This is updated after creating the boleto
	UpdatedAt           time.Time           `json:"updatedAt,omitempty" bson:"updatedAt,omitempty"`
	CreatedAt           time.Time           `json:"createdAt,omitempty" bson:"createdAt,omitempty"`
	MerchantName        string              `json:"-" bson:"merchantName,omitempty"`
}

func (p Payment) Validate() error {
	err := validation.Errors{
		"wirecardPayId":                   validation.Validate(&p.WirecardPayId, validation.Required, validation.Length(1, 45)),
		"ownId":                           validation.Validate(&p.OwnId, validation.Required, validation.Length(1, 45)),
		"status":                          validation.Validate(&p.Status, validation.Required, validation.In("CANCELLED")),
		"amount":                          validation.Validate(&p.Amount, validation.Required),
		"amount.total":                    validation.Validate(&p.Amount.Total, validation.Required),
		"cancellationDetails.cancelledBy": validation.Validate(&p.CancellationDetails.CancelledBy, validation.Required, validation.In("MOIP", "ACQUIRER")),
	}.Filter()

	// Gets fields that have error
	var errFields string
	if err != nil {
		errFields = utils.GetErrorFields(err)
		return fxerror.NewError("invalid-field", fxerror.InvalidField, errFields, "", http.StatusBadRequest, fxerror.InvalidRequest, err)
	}

	return nil
}

type PaymentStringTime struct {
	Id                  primitive.ObjectID  `json:"_id,omitempty" bson:"_id,omitempty"`
	WirecardPayId       string              `json:"wirecardPayId,omitempty" bson:"wirecardPayId,omitempty"`
	OwnId               string              `json:"ownId,omitempty" bson:"ownId,omitempty"`
	Status              string              `json:"status,omitempty" bson:"status,omitempty"`
	Amount              Amount              `json:"amount,omitempty" bson:"amount,omitempty"`
	CancellationDetails CancellationDetails `json:"cancellationDetails,omitempty" bson:"cancellationDetails,omitempty"`
	Device              Device              `json:"device,omitempty" bson:"device,omitempty"`
	Fluxx               Fluxx               `json:"fluxx,omitempty" bson:"fluxx,omitempty"`   // This is updated after sending webhook
	Boleto              BoletoDetails       `json:"boleto,omitempty" bson:"boleto,omitempty"` // This is updated after creating the boleto
	UpdatedAt           string              `json:"updatedAt,omitempty" bson:"updatedAt,omitempty"`
	CreatedAt           string              `json:"createdAt,omitempty" bson:"createdAt,omitempty"`
	MerchantName        string              `json:"merchantName,omitempty" bson:"merchantName,omitempty"`
}

// This is the item that is shown in the webhook app
type PendingPayment struct {
	Id       primitive.ObjectID `json:"id,omitempty"`
	Name     string             `json:"name,omitempty"`
	Cpf      string             `json:"cpf,omitempty"`
	OwnId    string             `json:"ownId,omitempty"`
	Total    int32              `json:"total,omitempty"`
	Merchant Merchant           `json:"merchant,omitempty"`
}
