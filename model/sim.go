package model

type SimLoan struct {
	UUID                    string       `json:"uuid,omitempty" bson:"uuid,omitempty"`
	Token                   string       `json:"token,omitempty" bson:"token,omitempty"`
	SessionId               string       `json:"sessionId,omitempty" bson:"sessionId,omitempty"`
	Status                  string       `json:"status,omitempty" bson:"status,omitempty"`
	NotificationSent        bool         `json:"notificationSent" bson:"notificationSent"`
	ProposalId              int          `json:"simProposalId,omitempty" bson:"simProposalId,omitempty"` // Set after capture
	Institution             *Institution `json:"institution,omitempty" bson:"institution,omitempty"`
	Offers                  []SimOffer   `json:"offers" bson:"offers,omitempty"`
	SelectedOffer           string       `json:"selectedOffer,omitempty" bson:"selectedOffer,omitempty"`
	ConditionalAvailability bool         `json:"conditionalAvailability" bson:"conditionalAvailability"`
	ConditionalValue        float64      `json:"conditionalValue,omitempty" bson:"conditionalValue,omitempty"`
	ConditionalInfo         []string     `json:"conditionalInfo,omitempty" bson:"conditionalInfo,omitempty"`
}

type SimOffer struct {
	UUID                 string  `json:"uuid" bson:"uuid"`
	InstallmentValue     float64 `json:"installmentValue" bson:"installmentValue"`                             // InstallmentValue
	TotalFinancedValue   float64 `json:"totalFinancedValue" bson:"totalFinancedValue"`                         // LoanValue
	InstallmentAmount    int64   `json:"installmentAmount" bson:"installmentAmount"`                           // NumberOfInstallments
	FirstInstallmentDate string  `json:"firstInstallmentDate,omitempty" json:"firstInstallmentDate,omitempty"` // LoanDate
	CETMensal            float64 `json:"cETMensal" bson:"cETMensal"`                                           // MonthlyInterestRate
	CETAnual             float64 `json:"cETAnual" bson:"cETAnual"`                                             // AnnualInterestRate
	TotalTaxes           float64 `json:"totalTaxes" bson:"totalTaxes"`
	OfferDescription     string  `json:"offerDescription,omitempty" bson:"offerDescription,omitempty"`
}
