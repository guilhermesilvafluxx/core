package model

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type ClientNotification struct {
	Id                  primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	NotificationId      string             `json:"notificationId,omitempty" bson:"notificationId,omitempty"`
	MerchantId          string             `json:"merchantId,omitempty" bson:"merchantId,omitempty"`
	ProposalId          primitive.ObjectID `json:"proposalId,omitempty" bson:"proposalId,omitempty"`
	NotificationType    string             `json:"notificationType,omitempty" bson:"notificationType,omitempty"`       // SMS, Email
	NotificationContext string             `json:"notificationContext,omitempty" bson:"notificationContext,omitempty"` // RECOVERY_SIGNIN, RECOVERY_PARTNER, CONTRACT_CONFIRM
	NotificationRetry   int64              `json:"notificationRetry,omitempty" bson:"notificationRetry,omitempty"`     // 1 to 3
	Partner             string             `json:"partner,omitempty" bson:"partner,omitempty"`                         // Used to track which partner triggered the notification

	ClientId    string      `json:"clientId,omitempty" bson:"clientId,omitempty"`
	Fullname    string      `json:"fullname,omitempty" bson:"fullname,omitempty"`
	Email       string      `json:"email,omitempty" bson:"email,omitempty"`
	Cellphone   Phone       `json:"cellphone,omitempty" bson:"cellphone,omitempty"`
	TaxDocument TaxDocument `json:"taxDocument,omitempty" bson:"taxDocument,omitempty"`
	CreatedAt   time.Time   `json:"createdAt,omitempty" bson:"createdAt,omitempty"` // Data de envio da notificacao
}
