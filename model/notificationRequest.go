package model

type NotificationRequest struct {
	FullName   string          `json:"fullName"`
	Email      string          `json:"email"`
	ClientId   string          `json:"clientId"`
	Partner    string          `json:"partner"`
	ProposalId string          `json:"proposalId"`
	Offers     []ProposalOffer `json:"offers"`
}
