package model

import (
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
	"bitbucket.org/guilhermesilvafluxx/core/fxerror"
	"bitbucket.org/guilhermesilvafluxx/core/utils"
	"net/http"
	"time"
)

type PreApproval struct {
	Id           string    `json:"id,omitempty" bson:"id,omitempty"`
	Status       string    `json:"status,omitempty" bson:"status,omitempty"`
	CreatedAt    time.Time `json:"createdAt,omitempty" bson:"createdAt,omitempty"`
	UpdatedAt    time.Time `json:"updatedAt,omitempty" bson:"updatedAt,omitempty"`
	Amount       Amount    `json:"amount,omitempty" bson:"amount,omitempty"`
	Customer     Customer  `json:"customer,omitempty" bson:"customer,omitempty"`
	Merchant     Merchant  `json:"merchant,omitempty" bson:"merchant,omitempty"`
	MerchantName string    `json:"-" bson:"merchantName,omitempty"`
}

// Retorna a informação de pré-aprovação do usuário
// Pode ser consultado pelo Id da Fluxx ou pelo CPF do consumidor
type PreApprovalData struct {
	Status   string   `json:"status,omitempty" bson:"status,omitempty"`
	AuthHold int32    `json:"auth_hold,omitempty" bson:"auth_hold,omitempty"`
	Amount   int32    `json:"amount,omitempty" bson:"amount,omitempty"`
	Customer Customer `json:"customer,omitempty" bson:"customer,omitempty"`
}

func (p PreApproval) Validate() error {
	err := validation.Errors{
		"amount":                          validation.Validate(&p.Amount, validation.Required),
		"customer":                        validation.Validate(&p.Customer, validation.Required),
		"merchant":                        validation.Validate(&p.Merchant, validation.Required),
		"merchant.confirmationUrl":        validation.Validate(&p.Merchant.ConfirmationURL, validation.Required, is.URL),
		"merchant.userConfirmationAction": validation.Validate(&p.Merchant.UserConfirmationAction, validation.In("POST", "GET")),
	}.Filter()

	// Gets fields that have error
	var errFields string
	if err != nil {
		errFields = utils.GetErrorFields(err)
		return fxerror.NewError("invalid-field", fxerror.InvalidField, errFields, "", http.StatusBadRequest, fxerror.InvalidRequest, err)
	}

	return nil
}
