package model

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type User struct {
	Id              primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	MerchantId      string             `json:"merchantId,omitempty" bson:"merchantId,omitempty"`
	Username        string             `json:"username,omitempty" bson:"username,omitempty"`
	Password        string             `json:"password,omitempty" bson:"password,omitempty"`
	Name            string             `json:"name,omitempty" bson:"name,omitempty"`
	BackgroundImage string             `json:"backgroundImageUrl,omitempty" bson:"backgroundImageUrl,omitempty"`
	LogoUrl         string             `json:"logoUrl,omitempty" bson:"logoUrl,omitempty"`
}
