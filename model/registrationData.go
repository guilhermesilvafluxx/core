package model

type RegistrationData struct {
	ClientId                 string `json:"clientId,omitempty"`
	Name                     string `json:"name"`
	Birthdate                string `json:"birthdate"`
	RG                       string `json:"rg"`
	UF                       string `json:"uf"`
	EmissionDate             string `json:"emissionDate"`
	DispatcherOrgan          string `json:"dispatcherOrgan"`
	Gender                   string `json:"gender"`
	MaritalState             string `json:"maritalState"`
	Phone                    string `json:"phone"`
	Cellphone                string `json:"cellphone"`
	MotherName               string `json:"motherName"`
	PoliticallyExposedPerson bool   `json:"politicallyExposedPerson"`
	Annotaded                bool   `json:"annotaded"`
	Deceased                 bool   `json:"deceased"`

	Salary     string `json:"salary"`
	Occupation string `json:"occupation"`
	Scholarity string `json:"scholarity"`
	Employment string `json:"employment"`
	JobTitle   string `json:"jobTitle"`
	Company    string `json:"company"`

	ZipCode      string `json:"zipCode"`
	Address      string `json:"address"`
	Number       string `json:"number"`
	Complement   string `json:"complement"`
	Neighborhood string `json:"neighborhood"`
	City         string `json:"city"`
	State        string `json:"state"`
	ResidentType string `json:"residentType"`

	Bank          string `json:"bank"`
	BankBranch    string `json:"bankBranch"`
	BranchDigit   string `json:"branchDigit"`
	AccountNumber string `json:"accountNumber"`
	AccountDigit  string `json:"accountDigit"`
	AccountType   string `json:"accountType"`
	Payday        string `json:"payday"`
	Risk          string `json:"risk"`

	LoanPeriod  int64       `json:"loanPeriod,omitempty"`
	Items       []Item      `json:"items,omitempty"`
	Amount      Amount      `json:"amount,omitempty"`
	TaxDocument TaxDocument `json:"taxDocument,omitempty"`
	Email       string      `json:"email"`
	FirebaseUID string      `json:"firebase_uid,omitempty"`
	UtmSource   string      `json:"utm_source"`
}

func (r RegistrationData) HasEnoughData() bool {

	emptyAmount := Amount{}
	emptyTax := TaxDocument{}

	if r.Name != "" && r.Birthdate != "" && r.RG != "" && r.UF != "" && r.EmissionDate != "" && r.DispatcherOrgan != "" &&
		r.Gender != "" && r.MaritalState != "" && r.Cellphone != "" && r.MotherName != "" && r.Salary != "" &&
		r.Occupation != "" && r.Scholarity != "" && r.Employment != "" && r.JobTitle != "" && r.ZipCode != "" &&
		r.Address != "" && r.Number != "" && r.Complement != "" && r.Neighborhood != "" && r.City != "" && r.State != "" &&
		r.ResidentType != "" && r.Bank != "" && r.BankBranch != "" && r.AccountNumber != "" && r.Amount != emptyAmount &&
		r.TaxDocument != emptyTax && r.Email != "" {

		return true
	} else {
		return false
	}
}

func (r RegistrationData) HasEnoughSimData() bool {

	emptyAmount := Amount{}
	emptyTax := TaxDocument{}

	if r.Name != "" && r.Birthdate != "" && r.Cellphone != "" && r.Email != "" && r.TaxDocument != emptyTax && r.Amount != emptyAmount {
		return true
	} else {
		return false
	}
}
