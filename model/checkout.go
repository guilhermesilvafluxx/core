package model

import (
	"errors"
	"github.com/Nhanderu/brdoc"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"regexp"
	"strconv"
	"time"
)

type Checkout struct {
	Id          string           `json:"-" bson:"_id,omitempty"`
	UUID        string           `json:"uuid,omitempty" bson:"uuid,omitempty"`           // Fluxx checkout ID
	OwnId       string           `json:"ownId,omitempty" bson:"ownId,omitempty"`         // Updated after patch
	OfferUUID   string           `json:"offerUUID,omitempty" bson:"offerUUID,omitempty"` // Updated after patch
	Status      string           `json:"status,omitempty" bson:"status,omitempty"`       // accepted, captured, confirmed, rejected
	Customer    CheckoutCustomer `json:"customer,omitempty" bson:"customer,omitempty"`
	Items       []Item           `json:"items,omitempty" bson:"items,omitempty"`
	Amount      Amount           `json:"amount,omitempty" bson:"amount,omitempty"`
	Merchant    Merchant         `json:"merchant,omitempty" bson:"merchant,omitempty"`
	RedirectURL string           `json:"redirectUrl,omitempty" bson:"redirectUrl,omitempty"`
	CreatedAt   time.Time        `json:"createdAt,omitempty" bson:"createdAt,omitempty"`
	UpdatedAt   time.Time        `json:"updatedAt,omitempty" bson:"updatedAt,omitempty"`
}

type CheckoutCustomer struct {
	FullName         string        `json:"fullName,omitempty" bson:"fullName,omitempty"`
	Email            string        `json:"email,omitempty" bson:"email,omitempty"`
	Gender           string        `json:"gender,omitempty" bson:"gender,omitempty"`       // M, F, O
	BirthDate        string        `json:"birthDate,omitempty" bson:"birthDate,omitempty"` // "1990-10-22"
	BirthCity        string        `json:"birthCity,omitempty" bson:"birthCity,omitempty"`
	BirthState       string        `json:"birthState,omitempty" bson:"birthState,omitempty"`
	TaxDocument      []TaxDocument `json:"taxDocument,omitempty" bson:"taxDocument,omitempty"`
	Phone            []Phone       `json:"phone,omitempty" bson:"phone,omitempty"`
	BillingAddresses []Address     `json:"billingAddresses,omitempty" bson:"billingAddresses,omitempty"`
	ShippingAddress  Address       `json:"shippingAddress,omitempty" bson:"shippingAddress,omitempty"`
}

type Institution struct {
	BankCode int    `json:"bankCode,omitempty" bson:"bankCode,omitempty"`
	BankName string `json:"bankName,omitempty" bson:"bankName,omitempty"`
}

type CheckoutUpdate struct {
	OwnId     string `json:"ownId,omitempty" bson:"ownId,omitempty"`
	OfferUUID string `json:"offerUUID,omitempty" bson:"offerUUID,omitempty"`
}

type CheckoutPrepare struct {
	Name       string `json:"name,omitempty" bson:"name,omitempty"`
	Email      string `json:"email,omitempty" bson:"email,omitempty"`
	BirthDate  string `json:"dateOfBirth,omitempty" bson:"dateOfBirth,omitempty"`
	CPF        string `json:"cpf,omitempty" bson:"cpf,omitempty"`
	Phone      CellPhone  `json:"phone,omitempty" bson:"phone,omitempty"`
	Amount     Amount `json:"amount,omitempty" bson:"amount,omitempty"`
	MotherName string `json:"motherName,omitempty" bson:"motherName,omitempty"`
	Gender     string `json:"gender,omitempty" bson:"gender,omitempty"`
}

func (p CheckoutPrepare) Validate() error {
	return validation.ValidateStruct(&p,
		validation.Field(&p.Name, validation.Required.Error("field required")),
		validation.Field(&p.Email, validation.Required.Error("field required")),
		//TODO Tirar porque no magento ta mandando mês e dia trocado validation.Field(&p.BirthDate, validation.Match(regexp.MustCompile(`^([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))$`)).Error("format must be YYYY-MM-DD")),
		validation.Field(&p.CPF, validation.Required.Error("field required"), validation.By(checkCPF)),
		validation.Field(&p.Phone),
		validation.Field(&p.Amount),
	)
}

func (p Phone) Validate() error {
	return validation.ValidateStruct(&p,
		validation.Field(&p.CountryCode, validation.Required.Error("field required")),
		validation.Field(&p.AreaCode, validation.Required.Error("field required"), validation.Min(11).Error("invalid area code"), validation.Max(99).Error("invalid area code")),
		validation.Field(&p.Number, validation.Required.Error("field required"), validation.By(checkPhoneNumber)),
	)
}

func (p CellPhone) Validate() error {
	return validation.ValidateStruct(&p,
		validation.Field(&p.CountryCode, validation.Required.Error("field required")),
		validation.Field(&p.AreaCode, validation.Required.Error("field required"), validation.Min(11).Error("invalid area code"), validation.Max(99).Error("invalid area code")),
		validation.Field(&p.Number, validation.Required.Error("field required"), validation.By(checkCellPhoneNumber)),
	)
}

func (c CheckoutCustomer) Validate() error {
	return validation.ValidateStruct(&c,
		validation.Field(&c.FullName, validation.Required.Error("field required")),
		validation.Field(&c.Email, validation.Required.Error("field required")),
		validation.Field(&c.Gender, validation.In("M", "F", "O").Error("must be 'M', 'F' or 'O'")),
		validation.Field(&c.BirthDate, validation.Match(regexp.MustCompile(`^([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01])$)`)).Error("format must be YYYY-MM-DD")),
		validation.Field(&c.BirthCity, validation.Required.Error("field required")),
		validation.Field(&c.BirthState, validation.Required.Error("field required")),
		validation.Field(&c.TaxDocument, validation.Required.Error("field required"), validation.Each(validation.By(checkTaxDocument))),
		validation.Field(&c.Phone, validation.Required.Error("field required"), validation.Each(validation.By(checkPhone))),
		validation.Field(&c.BillingAddresses, validation.Required.Error("field required")),
		validation.Field(&c.ShippingAddress, validation.Required.Error("field required")),
	)
}

func (o Checkout) Validate() error {
	return validation.ValidateStruct(&o,
		validation.Field(&o.Status, validation.In("accepted", "captured", "confirmed", "rejected")),
		validation.Field(&o.Customer),
		validation.Field(&o.Amount),
		validation.Field(&o.Merchant),
		validation.Field(&o.OfferUUID),
	)
}

// Response ao ler informações de checkout
type CheckoutData struct {
	Status    string   `json:"status,omitempty" bson:"status,omitempty"`
	CreatedAt string   `json:"created,omitempty" bson:"created,omitempty"`
	OwnId     string   `json:"ownId,omitempty" bson:"ownId,omitempty"`
	Amount    Amount   `json:"amount,omitempty" bson:"amount,omitempty"`
	Items     []Item   `json:"items,omitempty" bson:"items,omitempty"`
	Customer  Customer `json:"customer,omitempty" bson:"customer,omitempty"`
}

func checkCPF(value interface{}) error {
	document := value.(string)

	if brdoc.IsCPF(document) {
		return nil
	} else if document == "" {
		return nil
	}

	return errors.New("must be a valid CPF")
}

func checkTaxDocument(value interface{}) error {
	document := value.(TaxDocument)

	//rgxRG := regexp.MustCompile(`^(\d{1,2})(\d{7})$`)
	if document.Type == "CPF" && brdoc.IsCPF(document.Number) {
		return nil
	} else if document.Type == "CNPJ" && brdoc.IsCNPJ(document.Number) {
		return nil
	} else if document.Type == "RG" {
		return nil
	} else {
		return errors.New("invalid document")
	}
}

func checkPhoneNumber(value interface{}) error {
	phone := value.(int32)

	rgxPhone := regexp.MustCompile(`^([2-8]\d{7})|9[1-9]\d{7}$`)

	if rgxPhone.MatchString(strconv.Itoa(int(phone))) {
		return nil
	}

	return errors.New("must be a valid phone number")
}

func checkCellPhoneNumber(value interface{}) error {
	phone := value.(int32)

	rgxPhone := regexp.MustCompile(`9[1-9]\d{7}`)

	if rgxPhone.MatchString(strconv.Itoa(int(phone))) {
		return nil
	}

	return errors.New("must be a valid cellphone number")
}

func checkPhone(value interface{}) error {
	phone := value.(Phone)
	number := strconv.Itoa(int(phone.Number))

	rgxPhone := regexp.MustCompile(`^([2-8]\d{7})|9[1-9]\d{7}$`)

	if phone.AreaCode >= 11 && phone.AreaCode <= 99 && rgxPhone.MatchString(number) {
		return nil
	}

	return errors.New("must be a valid phone number")
}
