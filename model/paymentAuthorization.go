package model

import (
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"bitbucket.org/guilhermesilvafluxx/core/fxerror"
	"bitbucket.org/guilhermesilvafluxx/core/utils"
	"net/http"
)

// Body do request para autorizar pagamento, cria um charge
type PaymentAuthRequest struct {
	CheckoutToken string `json:"checkout_token,omitempty" bson:"checkout_token,omitempty"`
	OwnId         string `json:"ownId,omitempty" bson:"ownId,omitempty"`
	MerchantName  string `json:"merchantName,omitempty" bson:"merchantName,omitempty"`
}

// Informações de um charge
type PaymentAuthData struct {
	Id                   string   `json:"id,omitempty" bson:"id,omitempty"`
	Created              string   `json:"created,omitempty" bson:"created,omitempty"`
	Currency             string   `json:"currency,omitempty" bson:"currency,omitempty"`
	AuthHold             int32    `json:"auth_hold,omitempty" bson:"auth_hold,omitempty"`
	Payable              int32    `json:"payable" bson:"payable"`
	Void                 bool     `json:"void" bson:"void"`
	Expires              string   `json:"expires,omitempty" bson:"expires,omitempty"`
	Events               []Event  `json:"events,omitempty" bson:"events,omitempty"`
	OwnId                string   `json:"ownId,omitempty" bson:"ownId,omitempty"`
	Amount               Amount   `json:"amount,omitempty" bson:"amount,omitempty"`
	Items                []Item   `json:"items,omitempty" bson:"items,omitempty"`
	Customer             Customer `json:"customer,omitempty" bson:"customer,omitempty"`
	ShippingConfirmation string   `json:"shipping_confirmation,omitempty" bson:"shipping_confirmation,omitempty"`
	ShippingCarrier      string   `json:"shipping_carrier,omitempty" bson:"shipping_carrier,omitempty"`
}

func (p PaymentAuthRequest) Validate() error {
	err := validation.Errors{
		"checkout_token": validation.Validate(&p.CheckoutToken, validation.Required),
		"ownId":          validation.Validate(&p.OwnId, validation.Required, validation.Length(1, 45)),
	}.Filter()

	// Gets fields that have error
	var errFields string
	if err != nil {
		errFields = utils.GetErrorFields(err)
		return fxerror.NewError("invalid-field", fxerror.InvalidField, errFields, "", http.StatusBadRequest, fxerror.InvalidRequest, err)
	}

	return nil
}

type Details struct {
	Items          []Item   `json:"items,omitempty" bson:"items,omitempty"`
	OrderId        string   `json:"order_id,omitempty" bson:"order_id,omitempty"`
	ShippingAmount string   `json:"shipping_amount,omitempty" bson:"shipping_amount,omitempty"`
	Shipping       Shipping `json:"shipping,omitempty" bson:"shipping,omitempty"`
}

type Shipping struct {
	Name    Name    `json:"name,omitempty" bson:"name,omitempty"`
	Address Address `json:"address,omitempty" bson:"address,omitempty"`
}

type Name struct {
	Full string `json:"full,omitempty" bson:"full,omitempty"`
}
