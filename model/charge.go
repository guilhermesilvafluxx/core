package model

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

// Salvo no banco após autorizar pagamento
type Charge struct {
	Id                   string    `json:"id,omitempty" bson:"id,omitempty"` // charge_id !
	Created              time.Time `json:"created,omitempty" bson:"created,omitempty"`
	Currency             string    `json:"currency,omitempty" bson:"currency,omitempty"`
	AuthHold             int32     `json:"auth_hold,omitempty" bson:"auth_hold,omitempty"`
	Payable              int32     `json:"payable" bson:"payable"`
	Void                 bool      `json:"void" bson:"void"`
	Expires              time.Time `json:"expires,omitempty" bson:"expires,omitempty"`
	Events               []Event   `json:"events,omitempty" bson:"events,omitempty"`
	OwnId                string    `json:"ownId,omitempty" bson:"ownId,omitempty"`
	Amount               Amount    `json:"amount,omitempty" bson:"amount,omitempty"`
	Items                []Item    `json:"items,omitempty" bson:"items,omitempty"`
	Customer             Customer  `json:"customer,omitempty" bson:"customer,omitempty"`
	ShippingConfirmation string    `json:"shipping_confirmation,omitempty" bson:"shipping_confirmation,omitempty"`
	ShippingCarrier      string    `json:"shipping_carrier,omitempty" bson:"shipping_carrier,omitempty"`
	MerchantName         string    `json:"-" bson:"merchantName,omitempty"`
}

type Event struct {
	Created       time.Time          `json:"created,omitempty" bson:"created,omitempty"`
	Currency      string             `json:"currency,omitempty" bson:"currency,omitempty"`
	Id            primitive.ObjectID `json:"id,omitempty" bson:"id,omitempty"`
	TransactionId string             `json:"transaction_id,omitempty" bson:"transaction_id,omitempty"`
	Type          string             `json:"auth,omitempty" bson:"auth,omitempty"`
}
